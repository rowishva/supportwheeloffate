package com.astro.assignment;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT,
  classes = SupportWheelOfFateApplication.class)
@AutoConfigureMockMvc
public class SupportWheelOfFateApplicationTests {

	@Autowired
	private MockMvc mvc;
	 
	@Test
	public void testDashboard() throws Exception {
		
	    mvc.perform(get("/dashboard")
	    	.accept(MediaType.TEXT_HTML))
        	.andExpect(status().isOk())	  
        	.andExpect(view().name("dashboard"));	    
	}
}
