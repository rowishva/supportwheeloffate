package com.astro.assignment.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@MappedSuperclass
@EntityListeners(AbstractEntityListener.class)
public abstract class AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "CREATED_BY", updatable = false)
	@JsonIgnore
	protected String createdBy;

	@Column(name = "UPDATED_BY")
	@JsonIgnore
	protected String updatedBy;

	@Column(name = "CREATED_AT", updatable = false)
	@JsonIgnore
	protected Timestamp createdAt;

	@Column(name = "UPDATED_AT")
	@JsonIgnore
	protected Timestamp updatedAt;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}
