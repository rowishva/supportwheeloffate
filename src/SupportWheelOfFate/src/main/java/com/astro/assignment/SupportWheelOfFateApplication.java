package com.astro.assignment;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.astro.assignment.constant.Constant;
import com.astro.assignment.schedule.SupportTaskSchedule;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@SpringBootApplication
public class SupportWheelOfFateApplication {
	
	@Value("${support.task.scheduler.poolsize}")
	private int poolSize;
	
	@Autowired
	private SupportTaskSchedule supportTaskScheduleService;

	public static void main(String[] args) {
		SpringApplication.run(SupportWheelOfFateApplication.class, args);
	}	
	
	@Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler(){
        ThreadPoolTaskScheduler threadPoolTaskScheduler
          = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(poolSize);
        threadPoolTaskScheduler.setThreadNamePrefix(Constant.SUPPORT_TASK_SCHEDULE);
        return threadPoolTaskScheduler;
    }
	
	@PostConstruct
    public void scheduleSupportTask(){
		supportTaskScheduleService.schedule();
    }
}
