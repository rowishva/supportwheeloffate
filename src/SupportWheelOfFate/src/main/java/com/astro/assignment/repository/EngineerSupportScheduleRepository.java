package com.astro.assignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.astro.assignment.model.EngineerSupportSchedule;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
public interface EngineerSupportScheduleRepository extends JpaRepository<EngineerSupportSchedule, Long> {

}
