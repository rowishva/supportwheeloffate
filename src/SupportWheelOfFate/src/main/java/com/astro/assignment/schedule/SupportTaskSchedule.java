package com.astro.assignment.schedule;

import java.text.ParseException;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import com.astro.assignment.constant.Constant;
import com.astro.assignment.schedule.service.SupportTaskScheduleService;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@Service
public class SupportTaskSchedule {
	
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TaskScheduler taskScheduler;
	
	@Value("${support.task.scheduler.start.date}")
	private String startDate;
	
	@Value("${support.task.scheduler.delay.days}")
	private int fixedDelay;	
	
	@Autowired
	SupportTaskScheduleService service;
	
	public void schedule() {	
		Date date;
		try {
			date = Constant.FORMAT.parse(startDate);	
			Duration duration = Duration.ofDays(fixedDelay);
			Instant instant = date.toInstant();
			taskScheduler.scheduleAtFixedRate(new SupportTask(service), instant, duration);
		} catch (ParseException e) {
			LOG.error(""+e.getMessage());
		}		
	}

}
