package com.astro.assignment.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.astro.assignment.schedule.service.SupportTaskScheduleService;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
public class SupportTask implements Runnable{

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	private SupportTaskScheduleService service;
    
    public SupportTask(SupportTaskScheduleService service){
        this.service = service;
    }
     
    @Override
    public void run() {
    	LOG.info("Calling makeSuportShiftForNextWeeks()");    	
    	service.makeSuportShiftForNextWeeks();   
    	LOG.info("End makeSuportShiftForNextWeeks()");
    }
}
