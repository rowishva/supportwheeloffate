package com.astro.assignment.constant;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
public class Constant {
	
	public static final String SUPPORT_TASK_SCHEDULE = "Support-Task-Scheduler";
	public static final DateFormat FORMAT = new SimpleDateFormat("yyyyy-mm-dd");

}
