package com.astro.assignment.model;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
public enum SupportDayType {
	FIRST_HALF(1), SECOND_HALF(2);
	
	private Integer id;
	
	private SupportDayType(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
	
	public static SupportDayType getType(Integer id) {
	       
        if (id == null) {
            return null;
        }
 
        for (SupportDayType type : SupportDayType.values()) {
            if (id.equals(type.getId())) {
                return type;
            }
        }
        return null;
    }
}
