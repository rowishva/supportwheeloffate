package com.astro.assignment.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@Entity
@Table(name = "ENGINEER_SUPPORT_SCHEDULE")
public class EngineerSupportSchedule extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ENGINEER_SUPPORT_SCHEDULE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "SUPPORT_DATE")
	private LocalDate supportDate;
	
	@Column(name = "SUPPORT_DAY_TYPE", length = 30)
	@Enumerated(EnumType.STRING)
	private SupportDayType supportDayType;

	@ManyToOne
	@JoinColumn(name = "ENGINEER_ID")	
	private Engineer engineer;
	
	public EngineerSupportSchedule() {		
	}
	
	public EngineerSupportSchedule(LocalDate supportDate, SupportDayType supportDayType, Engineer engineer) {
		this.supportDate = supportDate;
		this.supportDayType = supportDayType;
		this.engineer = engineer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getSupportDate() {
		return supportDate;
	}

	public void setSupportDate(LocalDate supportDate) {
		this.supportDate = supportDate;
	}

	public SupportDayType getSupportDayType() {
		return supportDayType;
	}

	public void setSupportDayType(SupportDayType supportDayType) {
		this.supportDayType = supportDayType;
	}

	public Engineer getEngineer() {
		return engineer;
	}

	public void setEngineer(Engineer engineer) {
		this.engineer = engineer;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EngineerSupportSchedule [id=");
		builder.append(id);
		builder.append(", supportDate=");
		builder.append(supportDate);
		builder.append(", supportDayType=");
		builder.append(supportDayType);
		builder.append(", engineer=");
		builder.append(engineer);
		builder.append("]");
		return builder.toString();
	}

}
