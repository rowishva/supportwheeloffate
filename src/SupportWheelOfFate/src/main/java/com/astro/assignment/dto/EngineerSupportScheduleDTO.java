package com.astro.assignment.dto;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
public class EngineerSupportScheduleDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private LocalDate supportDate;	
	private String supportDayType;
	private String engineerName;
	
	public EngineerSupportScheduleDTO() {	
	}

	public EngineerSupportScheduleDTO(Long id, LocalDate supportDate, String supportDayType, String engineerName) {
		this.id = id;
		this.supportDate = supportDate;
		this.supportDayType = supportDayType;
		this.engineerName = engineerName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getSupportDate() {
		return supportDate;
	}

	public void setSupportDate(LocalDate supportDate) {
		this.supportDate = supportDate;
	}

	public String getSupportDayType() {
		return supportDayType;
	}

	public void setSupportDayType(String supportDayType) {
		this.supportDayType = supportDayType;
	}

	public String getEngineerName() {
		return engineerName;
	}

	public void setEngineerName(String engineerName) {
		this.engineerName = engineerName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EngineerSupportScheduleDTO [id=");
		builder.append(id);
		builder.append(", supportDate=");
		builder.append(supportDate);
		builder.append(", supportDayType=");
		builder.append(supportDayType);
		builder.append(", engineerName=");
		builder.append(engineerName);
		builder.append("]");
		return builder.toString();
	}
}
