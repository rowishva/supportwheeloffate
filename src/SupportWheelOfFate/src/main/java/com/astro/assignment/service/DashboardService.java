package com.astro.assignment.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astro.assignment.dto.EngineerSupportScheduleDTO;
import com.astro.assignment.model.EngineerSupportSchedule;
import com.astro.assignment.repository.EngineerSupportScheduleRepository;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@Service
public class DashboardService { 
	
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EngineerSupportScheduleRepository engineerSupportScheduleRepository;
	
	public List<EngineerSupportScheduleDTO> getSupportSchedule(){
		LOG.info("Calling getSupportSchedule() ");		
		List<EngineerSupportSchedule> scheduleList = engineerSupportScheduleRepository.findAll();
		List<EngineerSupportScheduleDTO> scheduleDTOList = new ArrayList<EngineerSupportScheduleDTO>();		
		scheduleList.forEach(schedule -> {
			String name = schedule.getEngineer().getFirstName() + " "+ schedule.getEngineer().getLastName();
			scheduleDTOList.add(new EngineerSupportScheduleDTO(schedule.getId(), schedule.getSupportDate(), schedule.getSupportDayType().toString(), name));});		
		return scheduleDTOList;		
	}
}
