package com.astro.assignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.astro.assignment.model.Engineer;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
public interface EngineerRepository extends JpaRepository<Engineer, Long> {
	
}
