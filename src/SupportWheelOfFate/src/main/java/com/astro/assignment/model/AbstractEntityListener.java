package com.astro.assignment.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
public class AbstractEntityListener {

	@PrePersist
	void prePersist(Object o) {

		if (o instanceof AbstractEntity) {
			AbstractEntity entity = (AbstractEntity) o;

			entity.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
			entity.setCreatedBy("Test");
		}

	}

	@PreUpdate
	void preUpdate(Object o) {
		if (o instanceof AbstractEntity) {
			AbstractEntity entity = (AbstractEntity) o;
			entity.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
			entity.setUpdatedBy("Test");
		}

	}

}
