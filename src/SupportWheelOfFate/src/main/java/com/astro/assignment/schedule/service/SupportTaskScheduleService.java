package com.astro.assignment.schedule.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astro.assignment.model.Engineer;
import com.astro.assignment.model.EngineerSupportSchedule;
import com.astro.assignment.model.SupportDayType;
import com.astro.assignment.repository.EngineerRepository;
import com.astro.assignment.repository.EngineerSupportScheduleRepository;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@Service
public class SupportTaskScheduleService {
	
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EngineerRepository engineerRepository;
	
	@Autowired
	private EngineerSupportScheduleRepository engineerSupportScheduleRepository;	
	
	public void makeSuportShiftForNextWeeks() {	
		List<Engineer> engineerFullList = engineerRepository.findAll();
		Map<Engineer, Integer> shiftCount = new HashMap<Engineer, Integer>();
		Queue<Engineer> waitingList = new LinkedList<Engineer>();
		List<EngineerSupportSchedule> shiftList = new ArrayList<EngineerSupportSchedule>();		
		LocalDate shiftStartDate = getNextMonday(LocalDate.now());
		int count = 0;
		do {			
			for (SupportDayType supportDayType : SupportDayType.values()) { 
				LocalDate shiftDate = getShiftDate(shiftStartDate, count);
				addWaitingListBack(waitingList, engineerFullList, count);			
				Engineer engineer = getRandomEngineer(engineerFullList);	
				shiftList.add(convertToEngineerSupportSchedule(engineer, shiftDate, supportDayType));
				checkCount(shiftCount, engineer, waitingList);
			}	
			count++;			
		}while(count < 10);
		saveScheduleList(shiftList);
	}
	
	private void addWaitingListBack(Queue<Engineer> waitingList, List<Engineer> engineerFullList, int count) {	
		if(waitingList.size() > 0 && count > 1) {
			engineerFullList.add(waitingList.remove());
		}		
	}
	
	private void checkCount(Map<Engineer, Integer> engineerCount, Engineer engineer, Queue<Engineer> waitingList) {	
		if(engineerCount.containsKey(engineer)) {
			engineerCount.put(engineer, 2);
							
		} else {
			engineerCount.put(engineer, 1);
			waitingList.add(engineer);
		}
	}
	
	private Engineer getRandomEngineer(List<Engineer> engineerFullList) {
		Random randomizer = new Random();
		return engineerFullList.remove(randomizer.nextInt(engineerFullList.size()));	
	}
	
	private EngineerSupportSchedule convertToEngineerSupportSchedule(Engineer engineer, LocalDate date, SupportDayType supportDayType) {	
		return new EngineerSupportSchedule(date, supportDayType, engineer);
	}
	
	private LocalDate getShiftDate(LocalDate shiftStartDate, int count) {	
		if(count > 4) {
			return shiftStartDate.plusDays(count + 2);
		}
		return shiftStartDate.plusDays(count);
	}
	
	private LocalDate getNextMonday(LocalDate localDate) {
		 return localDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
	}
	
	private void saveScheduleList(List<EngineerSupportSchedule> shiftList) {
		shiftList.forEach(engineerSupportSchedule -> LOG.info(engineerSupportSchedule.toString()));
		engineerSupportScheduleRepository.saveAll(shiftList);
	}
}
