package com.astro.assignment.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.astro.assignment.constant.ServiceEndpoint;
import com.astro.assignment.service.DashboardService;

/**
 * @author Roshitha Wishvajith <rowishva@gmail.com>
 *
 */
@Controller
public class DashboardController {
	
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DashboardService dashboardService;

	@RequestMapping(value = ServiceEndpoint.DASHBOARD, method = RequestMethod.GET)
	public ModelAndView dashboard() {
		LOG.info("Calling dashboard() ");
		ModelAndView model = new ModelAndView();
		model.addObject("scheduleList", dashboardService.getSupportSchedule());
		model.setViewName("dashboard");
		return model;
	}

}
