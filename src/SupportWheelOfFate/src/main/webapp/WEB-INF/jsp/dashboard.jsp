<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Engineer Support Schedule</title>
<link href="/bootstrap.min.css" rel="stylesheet">
    <script src="/jquery-2.2.1.min.js"></script>
    <script src="/bootstrap.min.js"></script>
</head>
<body>
<div>
<div class="container" style="margin:50px">
    <div class="row text-center"><strong> Engineer Support Schedule</strong></div>
    <div class="row" style="border:1px solid green;padding:10px">
        <div class="col-md-4 text-center"><strong>Name</strong></div>
        <div class="col-md-4 text-center"><strong>Support Date</strong></div>
        <div class="col-md-4 text-center"><strong>Shift</strong></div>
    </div>
        <c:forEach var="schedule" items="${scheduleList}">
            <div class="row" style="border:1px solid green;padding:10px">
            <div class="col-md-4 text-center">${schedule.engineerName}</div>
            <div class="col-md-4 text-center">${schedule.supportDate}</div>
            <div class="col-md-4 text-center">${schedule.supportDayType}</div>
            </div>
        </c:forEach>

</div>
</div>
</body>
</html>